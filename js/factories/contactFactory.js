
app.factory('Contact', function($resource){
	return $resource('https://api.codecraft.tv/samples/v1/contact/:id/', {id: '@id'}, {
        update:{
            method: 'PUT'
        }
    });
    //return $resource('https://randomuser.me/api/?results=500');
});