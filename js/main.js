var app = angular.module('codecraft',  [
	  'ngResource', 
 'infinite-scroll', 
  'angularSpinner', 
'jcs-autoValidate',
   'angular-ladda',
  'mgcrea.ngStrap',
  		 'toaster',
  	   'ngAnimate',
	   'ui.router'
	  ]);


app.run([
        'bootstrap3ElementModifier',
        function (bootstrap3ElementModifier) {
              bootstrap3ElementModifier.enableValidationStateIcons(true);
       }]);

app.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state(
			'list',{
			url:'/',
			templateUrl: 'templates/list.html',
			controller: 'PersonListController'
		} )
		.state(
			'edit',{
			url:'/edit/:email',
			templateUrl: 'templates/edit.html',
			controller: 'PersonDetailController'
		} )
		.state(
			'create',{
			url:'/create',
			templateUrl: 'templates/edit.html',
			controller: 'PersonCreateController'
		} );
		
		$urlRouterProvider.otherwise('/');
})

app.config(function($httpProvider, $resourceProvider, laddaProvider){
	$httpProvider.defaults.headers.common['Authorization'] = 'Token 41a551001b664f409ff7c4c6adec242b205f5528';	
	$resourceProvider.defaults.stripTrailingSlashes = false;
	//$resourceProvider.defaults.stripTrailingSlashes = true;
	laddaProvider.setOption({
		style: 'expand-right'
	});
});







