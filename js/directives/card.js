app.directive('ccCard', function () {
    return  {        
        'restrict': 'AE',
        'templateUrl': '../../templates/card.html',
        'scope':{
            'user':'=',
            'deleteUser':'&'
        }
    }
});