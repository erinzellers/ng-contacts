app.controller('PersonListController', function ($scope, $modal, ContactService) {

	$scope.search = "";
	$scope.order = "email";
	$scope.contacts = ContactService;

	$scope.$watch('search', function(newVal, oldVal){
        if (angular.isDefined(newVal)) {
            $scope.contacts.doSearch(newVal);
        }
    })

    $scope.$watch('order', function(newVal, oldVal){
        if (angular.isDefined(newVal)) {
            $scope.contacts.doOrder(newVal);
        }
    })

    $scope.loadMore = function(){
        console.log('Load More!!!');
        $scope.contacts.loadMore();
    };

    $scope.showCreateModal = function(){
        $scope.contacts.selectedPerson = {};
        $scope.createModal = $modal({
            scope: $scope,
            templateUrl: 'templates/modal.create.tpl.html',
            show: true
        })
    };

    $scope.createContact = function () {
        $scope.contacts.createContact($scope.contacts.selectedPerson)
        .then(function () {
            $scope.createModal.hide();
        });
    };

    $scope.parentDeleteUser = function (user) {
        $scope.contacts.removeContact(user);  
    };

});