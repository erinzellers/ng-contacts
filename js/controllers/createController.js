app.controller('PersonCreateController', function ($scope, ContactService, $stateParams, $state) {
	$scope.contacts = ContactService;
    $scope.mode = "Create";

    //$scope.contacts.selectedPerson = $scope.contacts.getPerson($stateParams.email);

    $scope.save = function(){
        $scope.contacts.createContact($scope.contacts.selectedPerson).then(function () {
            $state.go("list");
        });
    }

});