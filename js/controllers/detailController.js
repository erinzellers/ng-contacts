
app.controller('PersonDetailController', function ($scope, ContactService, $stateParams, $state) {
	$scope.contacts = ContactService;
    $scope.mode = "Edit";

    console.log($stateParams);

    $scope.contacts.selectedPerson = $scope.contacts.getPerson($stateParams.email);

    $scope.save = function(){
        $scope.contacts.updateContact($scope.contacts.selectedPerson).then(function () {
            $state.go("list");
        });
    }

    $scope.remove = function(){
        $scope.contacts.removeContact($scope.contacts.selectedPerson).then(function () {
            $state.go("list");
        });
    }

});
